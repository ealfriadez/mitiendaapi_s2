package pe.todotic.mitiendaapi_s2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MitiendaapiS2Application {

	public static void main(String[] args) {
		SpringApplication.run(MitiendaapiS2Application.class, args);
	}

}
