package pe.todotic.mitiendaapi_s2.web.dto;

import lombok.Data;

import javax.persistence.Entity;

@Data
public class LibroDTO {
    private String titulo;
    private String slug;
    private String descripcion;
    //private String rutaPortada;
    //private String rutaArchivo;
    private Float precio;
}
