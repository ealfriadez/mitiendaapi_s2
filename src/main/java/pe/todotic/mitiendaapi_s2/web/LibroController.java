package pe.todotic.mitiendaapi_s2.web;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pe.todotic.mitiendaapi_s2.model.Libro;
import pe.todotic.mitiendaapi_s2.repository.LibroRepository;
import pe.todotic.mitiendaapi_s2.web.dto.LibroDTO;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/libros")
public class LibroController {

    @Autowired
    private LibroRepository libroRepository;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/listar")
    List<Libro> listar() {
        return libroRepository.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    Libro obtener(@PathVariable Integer id) {
        return libroRepository
                .findById(id)
                .orElseThrow(EntityExistsException::new);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    Libro crear(@RequestBody LibroDTO  libroDTO) {

       // Libro libro = new Libro();
       // libro.setTitulo(libroDTO.getTitulo());
       // libro.setDescripcion(libroDTO.getDescripcion());
       // libro.setSlug(libroDTO.getSlug());
       // libro.setPrecio(libroDTO.getPrecio());

        Libro libro = new ModelMapper().map(libroDTO, Libro.class);

        return libroRepository.save(libro);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    Libro actualizar(
            @PathVariable Integer id,
            @RequestBody LibroDTO libroDTO
    ) {
        Libro libro = libroRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

       // libro.setSlug(libroDTO.getSlug());
       // libro.setDescripcion(libroDTO.getDescripcion());
       // libro.setPrecio(libroDTO.getPrecio());
       // libro.setTitulo(libroDTO.getTitulo());

        new ModelMapper().map(libroDTO, libro);

        return libroRepository.save(libro);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    void eliminar(@PathVariable Integer id) {
        Libro libro = libroRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        libroRepository.delete(libro);
    }

    /**
     * Devuelve la lista de libros de forma paginada.
     * El cliente puede enviar los parámetros page, size, sort,... en la URL
     * para configurar la página solicitada.
     * Si el cliente no envía ningún parámetro para la paginación,
     * se toma la configuración por defecto.
     * Retorna el status OK: 200
     * Ej.: GET http://localhost:9090/api/libros?page=0&size=2&sort=fechaCreacion,desc
     *
     * @param pageable la configuración de paginación que captura los parámetros como: page, size y sort
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    Page<Libro> index(
            @PageableDefault(sort = "titulo", direction = Sort.Direction.ASC, size = 3 ) Pageable pageable
    ) {
        return libroRepository.findAll(pageable);
    }

}
