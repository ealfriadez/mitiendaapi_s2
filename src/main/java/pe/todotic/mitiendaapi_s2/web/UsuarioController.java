package pe.todotic.mitiendaapi_s2.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.todotic.mitiendaapi_s2.model.Libro;
import pe.todotic.mitiendaapi_s2.model.Usuario;
import pe.todotic.mitiendaapi_s2.repository.UsuarioRepository;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    /**
     * Devuelve la lista completa de usuarios
     * Retorna el status OK: 200
     * Ej.: GET http://localhost:9090/api/usuarios
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/listar")
    List<Usuario> listar() {
        return usuarioRepository.findAll();
    }

    /**
     * Devuelve un usuario por su ID.
     * Retorna el status OK: 200
     * Ej.: GET http://localhost:9090/api/usuarios/1
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    Usuario obtener(@PathVariable Integer id) {
        return usuarioRepository
                .findById(id)
                .orElseThrow(EntityExistsException::new);
    }

    /**
     * Crea un usuario a partir del cuerpo
     * de la solicitud HTTP y retorna
     * el usuario creado.
     * Retorna el status CREATED: 201
     * Ej.: POST http://localhost:9090/api/usuarios
     */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    Usuario crear(@RequestBody Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    /**
     * Actualiza un usuario por su ID, a partir
     * del cuerpo de la solicitud HTTP.
     * Retorna el status OK: 200.
     * Ej.: PUT http://localhost:9090/api/usuarios/1
     */
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    Usuario actualizar(
            @PathVariable Integer id,
            @RequestBody Usuario usuarioForm
    ) {
        Usuario usuario = usuarioRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        usuario.setNombres(usuarioForm.getNombres());
        usuario.setApellidos(usuarioForm.getApellidos());
        usuario.setEmail(usuarioForm.getEmail());
        usuario.setPassword(usuarioForm.getPassword());
        usuario.setRol(usuarioForm.getRol());

        usuarioRepository.save(usuario);

        return usuario;
    }

    /**
     * Elimina un usuario por su ID.
     * Retorna el status NO_CONTENT: 204
     * Ej.: DELETE http://localhost:9090/api/usuarios/1
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    void eliminar(@PathVariable Integer id) {
        Usuario usuario = usuarioRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        usuarioRepository.delete(usuario);
    }

    /**
     * Devuelve la lista de usuarios de forma paginada.
     * El cliente puede enviar los parámetros page, size, sort,... en la URL
     * para configurar la página solicitada.
     * Si el cliente no envía ningún parámetro para la paginación,
     * se toma la configuración por defecto.
     * Retorna el status OK: 200
     * Ej.: GET http://localhost:9090/api/usuarios?page=0&size=2&sort=nombreCompleto,desc
     *
     * @param pageable la configuración de paginación que captura los parámetros como: page, size y sort
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    Page<Usuario> index(
            /* TODO 55: sobreescribir la config. por defecto:
             *   ordenar por el nombreCompleto de forma ascendente
             *   y con un tamaño de 10 elementos por página. */
           @PageableDefault(sort = "nombreCompleto", direction = Sort.Direction.ASC, size = 2 ) Pageable pageable
    ) {
        return usuarioRepository.findAll(pageable);
    }

}
